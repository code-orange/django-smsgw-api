from django.urls import path, include
from tastypie.api import Api

from django_smsgw_api.django_smsgw_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# API SMS
v1_api.register(SmsMessageResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
