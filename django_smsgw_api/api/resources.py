from django.forms import model_to_dict
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_smsgw_tasks.django_smsgw_tasks.tasks import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)


class SmsGatewayBaseResource(Resource):
    class Meta:
        always_return_data = True
        allowed_methods = ["get", "post"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return


class SmsMessageResource(SmsGatewayBaseResource):
    def obj_get(self, bundle, **kwargs):
        bundle = SmsGwMessage.objects.get(
            id=int(kwargs["pk"]), customer=bundle.request.user
        )

        return bundle

    def obj_get_list(self, bundle, **kwargs):
        bundle = SmsGwMessage.objects.filter(customer=bundle.request.user)

        return bundle

    def obj_create(self, bundle, **kwargs):
        new_message = SmsGwMessage(
            destination_nr=bundle.data["destination_nr"],
            text=bundle.data["text"],
            customer=bundle.request.user,
        )

        new_message.save()

        smsgw_process.apply_async()

        new_message.refresh_from_db()

        bundle.obj = new_message

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = model_to_dict(bundle.obj)

        return bundle
